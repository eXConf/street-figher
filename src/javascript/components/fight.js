import { controls } from '../../constants/controls';

const fighters = [];
let pressedKeys = [];

export async function fight(firstFighter, secondFighter) {
  document.body.addEventListener('keydown', onKeyEvent);
  document.body.addEventListener('keyup', onKeyEvent);

  setupFighters(firstFighter, secondFighter);

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    document.body.addEventListener('gameover', (e) => {
      const winner = e.detail.winner._id === firstFighter._id ? firstFighter : secondFighter;
      resolve(winner);
    });
  });
}

function setupFighters(firstFighter, secondFighter) {
  fighters.push(firstFighter, secondFighter);
  fighters[0].maxHealth = firstFighter.health;
  fighters[0].position = 'left';
  fighters[0].elem = document.querySelectorAll('.arena___left-fighter')[0];
  fighters[1].maxHealth = secondFighter.health;
  fighters[1].position = 'right';
  fighters[1].elem = document.querySelectorAll('.arena___right-fighter')[0];
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;

  return power;
}

function onKeyEvent({ type, code }) {
  switch (type) {
    case 'keydown':
      checkActionDown(code);
      if (!pressedKeys.includes(code)) {
        pressedKeys.push(code);
      }
      break;

    case 'keyup':
      checkActionUp(code);
      pressedKeys = pressedKeys.filter((value) => {
        return value === code ? null : value;
      });
      break;

    default:
      break;
  }
}

function checkActionUp(code) {
  if (code === controls.PlayerOneAttack) {
    if (pressedKeys.includes(controls.PlayerOneBlock)) return;
    if (pressedKeys.includes(controls.PlayerTwoBlock)) return animateAttack(fighters[0]);
    return attack(fighters[0], fighters[1]);
  }

  if (code === controls.PlayerTwoAttack) {
    if (pressedKeys.includes(controls.PlayerOneBlock)) return animateAttack(fighters[1]);
    if (pressedKeys.includes(controls.PlayerTwoBlock)) return;
    return attack(fighters[1], fighters[0]);
  }

  if (controls.PlayerOneCriticalHitCombination.includes(code)) {
    let critKeysPressed = 0;
    controls.PlayerOneCriticalHitCombination.forEach((critCode) => {
      critKeysPressed += pressedKeys.includes(critCode) ? 1 : 0;
    });

    if (critKeysPressed === controls.PlayerOneCriticalHitCombination.length) {
      critAttack(fighters[0], fighters[1]);
    }
    return;
  }

  if (controls.PlayerTwoCriticalHitCombination.includes(code)) {
    let critKeysPressed = 0;
    controls.PlayerTwoCriticalHitCombination.forEach((critCode) => {
      critKeysPressed += pressedKeys.includes(critCode) ? 1 : 0;
    });

    if (critKeysPressed === controls.PlayerTwoCriticalHitCombination.length) {
      critAttack(fighters[1], fighters[0]);
    }
    return;
  }

  if (code === controls.PlayerOneBlock) {
    return animateBlock(fighters[0], false);
  }

  if (code === controls.PlayerTwoBlock) {
    animateBlock(fighters[1], false);
  }
}

function checkActionDown(code) {
  if (code === controls.PlayerOneBlock) {
    animateBlock(fighters[0], true);
    return;
  }

  if (code === controls.PlayerTwoBlock) {
    animateBlock(fighters[1], true);
  }
}

function attack(attacker, defender) {
  animateAttack(attacker);
  defender.health -= getDamage(attacker, defender);

  updateHealthBars();
  checkGameOver();
}

function critAttack(attacker, defender) {
  const timeNow = new Date(Date.now());
  const rechargeTime = 10000; //ms

  if (attacker.lastCritTime && timeNow - attacker.lastCritTime <= rechargeTime) return;

  attacker.lastCritTime = new Date(Date.now());
  animateAttack(attacker, true);
  defender.health -= attacker.attack * 2;

  updateHealthBars();
  checkGameOver();
}

function updateHealthBars() {
  const bar1 = document.getElementById('left-fighter-indicator');
  const bar2 = document.getElementById('right-fighter-indicator');
  const bars = [bar1, bar2];

  bars.forEach((bar, id) => {
    let width = Math.floor((fighters[id].health / fighters[id].maxHealth) * 100);
    if (width < 0) {
      width = 0;
    }
    bar.style.width = width + '%';
  });
}

function animateAttack(fighter, isCrit) {
  const offset = 35;
  const speed = 100;
  const { elem, position } = fighter;

  if (position === 'left') {
    elem.style.left = offset + '%';
    setTimeout(() => {
      elem.style.left = 0;
    }, speed);
  } else if (position === 'right') {
    elem.style.right = offset + '%';
    setTimeout(() => {
      elem.style.right = 0;
    }, speed);
  }

  if (isCrit) {
    const arenaElem = document.getElementsByClassName('arena___root')[0];
    arenaElem.style.transform = 'scale(1.2)';
    setTimeout(() => {
      arenaElem.style.transform = 'scale(1)';
    }, 250);
  }
}

function animateBlock(fighter, isEnabled) {
  const { elem, position } = fighter;
  const offset = isEnabled ? -10 : 0;

  if (position === 'left') {
    elem.style.left = offset + '%';
  } else if (position === 'right') {
    elem.style.right = offset + '%';
  }
}

function checkGameOver() {
  let winner = null;

  if (fighters[0].health <= 0) {
    winner = fighters[1];
  } else if (fighters[1].health <= 0) {
    winner = fighters[0];
  }

  if (winner) {
    let event = new CustomEvent('gameover', {
      detail: {
        winner,
      },
    });

    document.body.dispatchEvent(event);
  }
}
