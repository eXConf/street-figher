import { showModal } from './modal';

export function showWinnerModal({ name, source }) {
  // call showModal function
  const image = new Image();
  image.src = source;

  showModal({
    title: `${name} won!`,
    bodyElement: image,
    onClose: () => window.location.reload(),
  });
}
