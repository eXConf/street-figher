import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const { attack, defense, health, name, source } = fighter;

    const imageElement = createElement({
      tagName: 'img',
      className: `fighter-preview-image ${position === 'right' ? 'flipped' : ''}`,
      attributes: {
        src: source,
      },
    });
    fighterElement.append(imageElement);

    const nameElement = createElement({
      tagName: 'div',
      className: 'fighter-preview-name',
    });
    nameElement.innerText = name;
    fighterElement.append(nameElement);

    const detailsElement = createElement({
      tagName: 'div',
      className: 'fighter-preview-details',
    });
    detailsElement.innerText = `Attack: ${'●'.repeat(attack)}
    Defense: ${'●'.repeat(defense)}
    Health: ${'●'.repeat(health / 15)}`;
    fighterElement.append(detailsElement);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
